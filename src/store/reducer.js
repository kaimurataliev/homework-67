
const initialState = {
    combination: '',
    access: false
};

const trueCombination = '1234';


const reducer = (state = initialState, action) => {
    switch(action.type) {
        case "PUT_NUMBER":
            if(state.combination.length < 4) {
                return {...state, combination: state.combination += action.value};
            }
            return state;

        case "CHECK_NUMBER":
            if(trueCombination === state.combination) {
                return {...state, access: true};
            } else {
                alert("Sorry, the password is incorrect")
            }
            return {...state, combination: state.combination};

        case "DELETE_NUMBER":
            return {...state, combination: state.combination.slice(0, -1)};

        case "HIDE_MODAL":
            return {...state, access: false};
    }
    return state;
};

export default reducer;