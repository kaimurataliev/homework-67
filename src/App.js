import React, { Component } from 'react';
import './App.css';
import Keypad from './Containers/Kaypad/Keypad';


class App extends Component {
  render() {
    return (
      <div className="App">
          <Keypad/>
      </div>
    );
  }
}

export default App;
