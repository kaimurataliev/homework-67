import React from 'react';
import './Modal.css';

const Modal = props => (
        <div className="Modal"
             style={{
                 transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                 opacity: props.show ? '1' : '0'
             }}
        >
            <h1>Access granted</h1>
            <button className="modal-ok-btn" onClick={props.hideModal}>OK</button>
        </div>
);

export default Modal;