import React, { Component, Fragment } from 'react';
import './Keypad.css';
import { connect } from 'react-redux';
import Modal from '../Modal/Modal';

class Keypad extends Component {
    render() {

        const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

        return (
            <Fragment>
                <Modal hideModal={this.props.hideModal} show={this.props.access}/>
                <input className={`show-combination ${this.props.access ? "green" : null}`} type="password" value={this.props.combination}/>
                {/*<input type="number" value={new Array(this.props.combination.length).fill('*').join('')}/>*/}
                <div className="flex-buttons">
                    {numbers.map((key, index) => {
                        return (
                            <div key={index} className="num-btn">
                                <button className="number" onClick={() => this.props.numHandler(key)}>{key}</button>
                            </div>
                        )
                    })}
                    <button className="sub-btn" onClick={this.props.checkNumber}>Enter</button>
                    <button className="sub-btn" onClick={this.props.deleteNumber}>Delete</button>
                </div>
            </Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        combination : state.combination,
        access: state.access
    }
};

const mapDispatchToProps = dispatch => {
    return {
        numHandler: (value) => dispatch({type: "PUT_NUMBER", value: value}),
        deleteNumber: () => dispatch({type: "DELETE_NUMBER"}),
        checkNumber: () => dispatch({type: "CHECK_NUMBER"}),
        hideModal: () => dispatch({type: "HIDE_MODAL"})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Keypad);